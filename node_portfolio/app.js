const express = require("express");
const dotenv = require("dotenv");
const nunjucks = require("nunjucks");
const morgan = require("morgan");
const methodOverride = require("method-override");
const path = require("path");
const { sequelize, Sequelize : { QueryTypes } } = require("./models")

/** 라우터 등록 */
const ratingRouter = require("./routes/rating");

const app = express();
dotenv.config();

app.set("port", process.env.PORT || 3000);
app.set("view engine", "html");
nunjucks.configure("views", {
	express : app,
	watch : true,
});

sequelize.sync({ force : false })
	.then(() => {
		console.log("데이터베이스 연결 성공");
	})
	.catch((err) => {
		console.error(err);
	})

app.use(morgan("dev"));
app.use(methodOverride("_method"));
app.use(express.static(path.join(__dirname, "public")));

// body-parser
app.use(express.json());
app.use(express.urlencoded({ extended : false }));

/** 라우터 */
app.use("/rating", ratingRouter);

// 없는 페이지 처리 미들웨어
app.use((req, res, next) => {
	const error = new Error(`${req.method} ${req.url}은 없는 페이지 입니다.`);
	error.status = 404;
	next(error);
});

// 에러 처리 미들웨어
app.use((err, req, res, next) => {
	res.locals.error = err;
	res.status(err.status || 500).render("error");
})

app.listen(app.get("port"), () => {
	console.log(app.get("port"), "번 포트에서 대기중");
})