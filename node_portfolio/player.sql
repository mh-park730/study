-- --------------------------------------------------------
-- 호스트:                          127.0.0.1
-- 서버 버전:                        10.5.9-MariaDB - mariadb.org binary distribution
-- 서버 OS:                        Win64
-- HeidiSQL 버전:                  11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- player 데이터베이스 구조 내보내기
CREATE DATABASE IF NOT EXISTS `player` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `player`;

-- 테이블 player.arsenal_fc 구조 내보내기
CREATE TABLE IF NOT EXISTS `arsenal_fc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `backNo` tinyint(4) NOT NULL,
  `playerNm` varchar(15) NOT NULL,
  `position` varchar(10) NOT NULL,
  `goal` varchar(3) DEFAULT ' ',
  `assist` varchar(3) DEFAULT ' ',
  `yellow` varchar(3) DEFAULT ' ',
  `red` varchar(3) DEFAULT ' ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;

-- 테이블 데이터 player.arsenal_fc:~14 rows (대략적) 내보내기
DELETE FROM `arsenal_fc`;
/*!40000 ALTER TABLE `arsenal_fc` DISABLE KEYS */;
INSERT INTO `arsenal_fc` (`id`, `backNo`, `playerNm`, `position`, `goal`, `assist`, `yellow`, `red`) VALUES
	(1, 1, '레노', 'GK', '', NULL, NULL, NULL),
	(2, 23, '루이스', 'DF', '', NULL, NULL, NULL),
	(3, 6, '마갈량이스', 'DF', '', NULL, NULL, NULL),
	(4, 17, '세드릭', 'DF', '', NULL, NULL, NULL),
	(5, 3, '티어니', 'DF', '', '1', NULL, NULL),
	(6, 8, '외데고르', 'MF', '1', NULL, NULL, NULL),
	(7, 18, '파티', 'MF', '', NULL, NULL, NULL),
	(8, 25, '엘네니', 'MF', '', NULL, NULL, NULL),
	(9, 32, '스미스 로우', 'MF', '', NULL, NULL, NULL),
	(10, 34, '자카', 'MF', '', NULL, '1', NULL),
	(11, 7, '사카', 'FW', '', NULL, NULL, NULL),
	(12, 9, '라카제트', 'FW', '1', NULL, NULL, NULL),
	(13, 12, '윌리안', 'FW', '', NULL, NULL, NULL),
	(14, 19, '페페', 'FW', '', NULL, NULL, NULL);
/*!40000 ALTER TABLE `arsenal_fc` ENABLE KEYS */;

-- 테이블 player.tottenham_hotspur_fc 구조 내보내기
CREATE TABLE IF NOT EXISTS `tottenham_hotspur_fc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `backNo` tinyint(4) NOT NULL,
  `playerNm` varchar(20) NOT NULL,
  `position` varchar(10) NOT NULL,
  `goal` int(11) DEFAULT NULL,
  `assist` int(11) DEFAULT NULL,
  `yellow` int(11) DEFAULT NULL,
  `red` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1113 DEFAULT CHARSET=utf8;

-- 테이블 데이터 player.tottenham_hotspur_fc:~14 rows (대략적) 내보내기
DELETE FROM `tottenham_hotspur_fc`;
/*!40000 ALTER TABLE `tottenham_hotspur_fc` DISABLE KEYS */;
INSERT INTO `tottenham_hotspur_fc` (`id`, `backNo`, `playerNm`, `position`, `goal`, `assist`, `yellow`, `red`) VALUES
	(1, 1, '요리스', 'GK', NULL, NULL, NULL, NULL),
	(2, 6, '산체스', 'DF', NULL, NULL, 1, NULL),
	(3, 4, '알더바이럴트', 'DF', NULL, NULL, NULL, NULL),
	(4, 3, '레길론', 'DF', NULL, NULL, 1, NULL),
	(5, 2, '도허티', 'DF', NULL, NULL, NULL, NULL),
	(6, 20, '알리', 'MF', NULL, NULL, NULL, NULL),
	(7, 5, '호이비에르', 'MF', NULL, NULL, NULL, NULL),
	(8, 27, '루카스 모우라', 'MF', NULL, 1, NULL, NULL),
	(9, 17, '시소코', 'MF', NULL, NULL, NULL, NULL),
	(10, 9, '베일', 'MF', NULL, NULL, NULL, NULL),
	(11, 28, '은돔벨레', 'MF', NULL, NULL, NULL, NULL),
	(12, 7, '손흥민', 'FW', NULL, NULL, NULL, NULL),
	(13, 10, '케인', 'FW', NULL, NULL, NULL, NULL),
	(14, 11, '라멜라', 'FW', 1, NULL, 2, 1);
/*!40000 ALTER TABLE `tottenham_hotspur_fc` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
