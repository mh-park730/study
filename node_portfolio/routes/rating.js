const express = require("express");
const router = express.Router();
const { sequelize, Sequelize : { QueryTypes } } = require('../models');

router.get('/', async (req, res, next) => {
	try {
		const ars = "SELECT backNo, playerNm, position, goal, assist, yellow, red FROM arsenal_fc ORDER BY id ASC";
		const ars_list = await sequelize.query(ars, {
			type : QueryTypes.SELECT,
		});
		
		const tot = "SELECT backNo, playerNm, position, goal, assist, yellow, red FROM tottenham_hotspur_fc ORDER BY id ASC";
		const tot_list = await sequelize.query(tot, {
			type : QueryTypes.SELECT,
		});
		
		res.render('rating', { ars_list, tot_list });
		
	} catch (err) {
		console.error(err);
		next(err);
	}
	
});

module.exports = router;